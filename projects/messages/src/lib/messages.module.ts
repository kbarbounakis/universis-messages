import { NgModule } from '@angular/core';
import { MessagesComponent } from './messages.component';

@NgModule({
  imports: [
  ],
  declarations: [MessagesComponent],
  exports: [MessagesComponent]
})
export class MessagesModule { }
